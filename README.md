If you are reading this, you definitely want to join our team, for this you should write last part of script to decrypt job description and 
get human job description.


### How it's working ###

For encryption we are using ```openssl_encrypt``` method to decrypt encrypted part you should use ```openssl_decrypt``` method in PHP.

### What you need to know? ###

* Initialization Vector size is ```16```
* Open_ssl cipher method is ```AES-256-CBC```
* Secret key is ```234eeb83486195311eb309d4feef1067```

### Getting Started ###

```
<?php

require_once('vendor/autoload.php');

$devManager = new Realtyna\Challenge\DevelopmentManager();
// You should finish this code to decrypt encrypted part 

```
