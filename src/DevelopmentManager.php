<?php

namespace Realtyna\Challenge;

class DevelopmentManager extends HiringManager {

    protected function makeInterviewer(): Realtyna {
        return new Developer();
    }
}
