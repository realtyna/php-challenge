<?php

namespace Realtyna\Challenge;

abstract class HiringManager {

    abstract protected function makeInterviewer(): Realtyna;

    public function takeInterview() {

        $interviewer = $this->makeInterviewer();
        return $interviewer->jobDescription();
    }
}
